# .files

Based on [webpro's dotfiles](https://github.com/webpro/dotfiles.git)

## Install

On a sparkling fresh installation of macOS:

```
sudo softwareupdate -i -a
xcode-select --install
```

Then install the dotfiles from local gitlab

```bash
cd ~
git clone git@gitlab.rete.farm:qa/dotfiles.git .dotfiles
source .dotfiles/install.sh
```

## Post install

* set your git user name `git config --global user.name "YOUR_NAME"`
* set yout git email `git config --global user.email YOUR_WORK_EMAIL`
* initialize git lfs `git lfs install`

## The `dotfiles` command

```
     $ dotfiles help
     Usage: dotfiles <command>

     Commands:
        clean            Clean up caches (brew, npm, gem, rvm)
        edit             Open dotfiles in IDE (atom) and Git GUI (stree)
        help             This help message
        update           Update packages and pkg managers (OS, brew, npm, gem)
```

## Customize/extend

You can put your custom settings, such as Git credentials in the `system/.custom` file which will be sourced from `.bash_profile` automatically. This file is in `.gitignore`.

Alternatively, you can have an additional, personal dotfiles repo at `~/.extra`.

* The runcom `.bash_profile` sources all `~/.extra/runcom/*.sh` files.
* The installer (`install.sh`) will run `~/.extra/install.sh`.


## Additional resources

* [Awesome Dotfiles](https://github.com/webpro/awesome-dotfiles)
* [Homebrew](http://brew.sh/) / [FAQ](https://github.com/Homebrew/homebrew/wiki/FAQ)
* [homebrew-cask](http://caskroom.io/) / [usage](https://github.com/phinze/homebrew-cask/blob/master/USAGE.md)
* [Bash prompt](http://wiki.archlinux.org/index.php/Color_Bash_Prompt)
* [Solarized Color Theme for GNU ls](https://github.com/seebi/dircolors-solarized)

## Credits

Many thanks to the [dotfiles community](http://dotfiles.github.io/) and the creators of the incredibly useful tools.
