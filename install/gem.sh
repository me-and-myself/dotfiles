#!/usr/bin/env bash

RUBY_VERSION=2.5

if ! is-macos -o ! is-executable brew; then
  echo "Skipped: gem"
  return
fi

brew install gpg2
command curl -sSL https://rvm.io/mpapis.asc | gpg --import -

export rvm_ignore_dotfiles=yes
command curl -sSL https://get.rvm.io | bash -s stable

# shellcheck source=/dev/null
[[ -s "$HOME/.rvm/scripts/rvm" ]] && source "$HOME/.rvm/scripts/rvm"

rvm install $RUBY_VERSION
rvm use $RUBY_VERSION --default

packages=(
  cucumber
  cocoapods
)

gem install "${packages[@]}"
