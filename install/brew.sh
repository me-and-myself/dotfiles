#!/usr/bin/env bash

# Install Homebrew

if ! is-macos -o ! is-executable ruby -o ! is-executable curl -o ! is-executable git; then
  echo "Skipped: Homebrew (missing: ruby, curl and/or git)"
  return
fi

ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"

brew update
brew upgrade

# Install packages

apps=(
  ansible
  ctags
  diff-so-fancy
  exiftool
  gauge
  geckodriver
  git
  git-extras
  git-lfs
  gradle
  groovy
  imagemagick
  jq
  maven
  shellcheck
  swagger-codegen
  tree
  unar
  wget
  xz
  yarn
)

brew install "${apps[@]}"
