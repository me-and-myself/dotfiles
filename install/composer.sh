#!/usr/bin/env bash

PHP_VERSION_BREW=7.2

if ! is-macos -o ! is-executable brew; then
  echo "Skipped: composer (not macos or missing brew)"
  return
fi

brew install php@$PHP_VERSION_BREW

PATH="$(brew --prefix php@$PHP_VERSION_BREW)/bin:$(brew --prefix php@$PHP_VERSION_BREW)/sbin:$PATH"

$PATH pecl install xdebug

USER_BIN_DIR="$HOME/.local/bin"

if ! [ -d "$USER_BIN_DIR" ] ; then
  mkdir -p "$USER_BIN_DIR"
fi

# Install composer executable
curl -sS https://getcomposer.org/installer | php -- --filename=composer --install-dir="$USER_BIN_DIR"

# Install symfony executable
curl -LsS http://symfony.com/installer -o "$USER_BIN_DIR/symfony" && chmod +x "$USER_BIN_DIR/symfony"

# Install php-cs-fixer
curl -LsS http://cs.sensiolabs.org/download/php-cs-fixer-v2.phar -o "$USER_BIN_DIR/php-cs-fixer" && chmod +x "$USER_BIN_DIR/php-cs-fixer"
