#!/usr/bin/env bash

NODE_VERSION=10

if ! is-executable brew -o ! is-executable git; then
  echo "Skipped: npm (missing: brew and/or git)"
  return
fi

brew install nvm

brew install yarn --without-node

DOTFILES_BREW_PREFIX_NVM=$(brew --prefix nvm)
export DOTFILES_BREW_PREFIX_NVM
set-config "DOTFILES_BREW_PREFIX_NVM" "$DOTFILES_BREW_PREFIX_NVM" "$DOTFILES_CACHE"

# shellcheck source=system/.nvm
. "${DOTFILES_DIR}/system/.nvm"
nvm install $NODE_VERSION

# Globally install with npm

packages=(
    #bower
    babel-cli
      babel-jest
      babel-preset-env
      babel-preset-react
      babel-polyfill
    browser-sync
    #coffee-script
    create-react-app
    eslint
      eslint-config-airbnb
      eslint-plugin-import
      eslint-plugin-jest
      eslint-plugin-jsx-a11y
      eslint-plugin-react
    gitbook-cli
    grunt-cli
    #grunt-init
    gulp-cli
    karma-cli
    semver
    superstatic
    supervisor
)

npm install -g "${packages[@]}"
