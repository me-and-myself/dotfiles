#!/usr/bin/env bash

# Get current dir (so run this script from anywhere)

export DOTFILES_DIR DOTFILES_CACHE DOTFILES_EXTRA_DIR
DOTFILES_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
DOTFILES_CACHE="$DOTFILES_DIR/.cache.sh"
DOTFILES_EXTRA_DIR="$HOME/.extra"

# Make utilities available

PATH="$DOTFILES_DIR/bin:$PATH"

# Update dotfiles itself first

if is-executable git -a -d "$DOTFILES_DIR/.git"; then git --work-tree="$DOTFILES_DIR" --git-dir="$DOTFILES_DIR/.git" pull origin master; fi

# Bunch of symlinks

ln -sfv "$DOTFILES_DIR/runcom/.bash_profile" ~
ln -sfv "$DOTFILES_DIR/runcom/.inputrc" ~
ln -sfv "$DOTFILES_DIR/runcom/.gemrc" ~
ln -sfv "$DOTFILES_DIR/git/.gitconfig" ~
ln -sfv "$DOTFILES_DIR/git/.gitignore_global" ~

# Package managers & packages

# shellcheck source=install/brew.sh
. "$DOTFILES_DIR/install/brew.sh"

# shellcheck source=install/npm.sh
. "$DOTFILES_DIR/install/npm.sh"

# shellcheck source=install/bash.sh
. "$DOTFILES_DIR/install/bash.sh"

#. "$DOTFILES_DIR/install/brew-cask.sh"

# shellcheck source=install/gem.sh
. "$DOTFILES_DIR/install/gem.sh"

# shellcheck source=install/composer.sh
. "$DOTFILES_DIR/install/composer.sh"

# Run tests

#if is-executable bats; then bats test/*.bats; else echo "Skipped: tests (missing: bats)"; fi

# Install extra stuff

if [ -d "$DOTFILES_EXTRA_DIR" ] && [ -f "$DOTFILES_EXTRA_DIR/install.sh" ]; then
  # shellcheck source=/dev/null
  . "$DOTFILES_EXTRA_DIR/install.sh"
fi

## Create local binary dir

USER_BIN_DIR="$HOME/.local/bin"

if ! [ -d "$USER_BIN_DIR" ] ; then
  mkdir -p "$USER_BIN_DIR"
fi
