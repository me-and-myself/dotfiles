#!/usr/bin/env bash
DOCKER_RESOURCES=/Applications/Docker.app/Contents/Resources/etc

BREW_PREFIX=$(brew --prefix)
BASH_COMPLETION="$BREW_PREFIX/share/bash-completion/completions"

if is-executable brew -a [ -d "$BASH_COMPLETION" ] ; then
  ln -s $DOCKER_RESOURCES/docker.bash-completion "$BASH_COMPLETION/docker"
  ln -s $DOCKER_RESOURCES/docker-machine.bash-completion "$BASH_COMPLETION/docker-machine"
  ln -s $DOCKER_RESOURCES/docker-compose.bash-completion "$BASH_COMPLETION/docker-compose"
fi

unset BREW_PREFIX BASH_COMPLETION DOCKER_RESOURCES
